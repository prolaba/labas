﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba3
{
    class MyStorage
    {
        const int N = 500;
        string[] name = new string[N];
        int[] razmer = new int[N];
        int index = 0;

        public MyStorage()
        {
            for (int i = 0; i < N; i++)
            {
                name[i] = "";
                razmer[i] = 0;
            }
        }

        public MyStorage(int count)
        {
            for (int i = 0; i < count; i++)
            {
                name[i] = "";
                razmer[i] = 0;
            }
        }

        public void AddItem(string ItemNam, int ItemRazm)
        {
            Console.WriteLine("Пытаемся добавить {0} с размером {1} ", ItemNam, ItemRazm);
            if (isItemUniq(ItemNam, ItemRazm) == true)
            {
                name[index] = ItemNam;
                razmer[index] = ItemRazm;
                index++;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Добавили {0} с размером {1} ", ItemNam, ItemRazm);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Такой элемент ({0} {1}) уже присутствует в ХРАНИЛИЩЕ",ItemNam,ItemRazm);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        public void DeleteItem(string ItemNam, int ItemRazm)
        {
            for (int i = 0; i < index; i++)
                if (name[i] == ItemNam && ItemRazm == razmer[i])
                {
                    name[i] = "";
                    razmer[i] = 0;

                    ArrayShift(i);
                    index--;
                    Console.WriteLine("Удалили {0} с размером {1} ", ItemNam, ItemRazm);
                    break;
                }
        }

        public bool isItemUniq(string ItemNam, int ItemRazm)
        {
            for (int i = 0; i < index; i++)
                if (ItemNam == name[i] && ItemRazm == razmer[i])
                    return false;
            return true;
        }

        public void ArrayShift(int DelItemIndex)
        {
            for (int i = DelItemIndex; i <= index; i++)
            {
                name[i] = name[i + 1];
                razmer[i] = razmer[i + 1];
            }
            name[index] = "";
            razmer[index] = 0;
        }

        public void ShowStorage()
        {
            for (int i = 0; i < index; i++)
                Console.WriteLine(name[i] + "  " + razmer[i]);
            Console.WriteLine();
        }
    }
}
